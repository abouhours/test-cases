/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  plus                                  |
|   \\  /    A nd           | Web:      www.OpenFOAM.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      controlDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

application     multiRegionPhaseChangeFlow;

startFrom       latestTime;

startTime       0;

stopAt          endTime;

endTime         2.5;

deltaT          1e-7;

writeControl    adjustableRunTime;

writeInterval   0.01;

purgeWrite      0;

writeFormat     ascii;

writePrecision  16;

writeCompression off;

timeFormat      general;

timePrecision   16;

runTimeModifiable yes;

adjustTimeStep  yes;

maxCo           0.1;
maxAlphaCo      0.1;
maxDeltaT       1;

functions
{
    // *********************************************************************** //
    // *** coded functionObject for calculating Berenson number ************** //
    // *********************************************************************** //

    NusseltNumber
    {
        // Load the library containing the 'coded' functionObject
        libs ("libutilityFunctionObjects.so");
        type coded;
        writeControl  adjustableRunTime;
        writeInterval 0.01;
        // Name of on-the-fly generated functionObject
        name NusseltNumber;
        region fluid;
        // Code
        codeWrite
        #{
            scalar sigma_ = 0.1; // if start not at the border
            scalar rhog_ = 5;
            scalar rhol_ = 200;
            scalar g_ = 9.81;
            scalar Hlg_ = 10e03;
            scalar kg_ = 1;
            scalar mug_ = 0.005;
            scalar DT_ = 5;
            scalar lambda_ = sqrt( sigma_/((rhol_-rhog_)*g_) );
            const volScalarField& T=mesh().lookupObject<volScalarField>("T");
            // Compute interface position
            scalar Nub_ = 0.425*pow(((rhog_*(rhol_-rhog_)*g_*Hlg_)/(kg_*mug_*abs(DT_))),0.25)*pow(lambda_,0.75);
            // Print interface position
            Info << "Berenson's correlation number = " << Nub_ << endl;
            label down = mesh().boundary().findPatchID("down");
            volScalarField Nusselt
            (
                IOobject
                (
                    "Nusselt",
                    mesh().time().timeName(),
                    mesh(),
                    IOobject::NO_READ,
                    IOobject::AUTO_WRITE
                ),
                mesh(),
                dimensionedScalar("Nusselt", dimless, 0.0)
            );
            Nusselt.boundaryFieldRef()[down] = lambda_/DT_*T.boundaryField()[down].snGrad();
            scalar area = gSum(mesh().magSf().boundaryField()[down]);
            scalar avgNusselt = gSum(Nusselt.boundaryField()[down] * mesh().magSf().boundaryField()[down])/area;
            Info << "Space-averaged Nusselt = " << avgNusselt << "\n" << endl;
        #};
    }

}


// ************************************************************************* //
