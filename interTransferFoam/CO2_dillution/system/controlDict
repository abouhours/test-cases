/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  1.6                                   |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      controlDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

application     interTransferFoamM;

startFrom       latestTime;

startTime       0;

stopAt          endTime;

endTime         0.25;

deltaT          1e-10;

writeControl    adjustableRunTime;

writeInterval   0.001;

purgeWrite      0;

writeFormat     binary;

writePrecision  16;

writeCompression no;

timeFormat      general;

timePrecision   6;

runTimeModifiable yes;

adjustTimeStep  yes;

maxCo           0.1;

maxAlphaCo      0.1;

maxDeltaT       1;


// ************************************************************************* //

functions
{
    // **************************************************** //
    // **** coded functionObject for bubble processing **** //
    // **************************************************** //
    bubblepproc
    {
        // Load the library containing the 'coded' functionObject
        libs ("libutilityFunctionObjects.so");
        type coded;
        writeControl        timeStep;
        writeInterval       1;
        // Name of on-the-fly generated functionObject
        name bubblepproc;
        // List of include search directories
        codeOptions
        #{
            -I$(LIB_SRC)/sampling/lnInclude \
            -I$(LIB_SRC)/surfMesh/lnInclude 
        #};
        // List of libraries to link with
        codeLibs
        #{
            -lsampling \
            -lsurfMesh 
        #};
        // List of include files
        codeInclude
        #{
            #include "sampledIsoSurfaceCell.H"
        #};
        // Code
        codeWrite
        #{
            Info << "----------bubblepproc----------" << endl;

            // Lookup velocity field U
            const volVectorField& U = mesh().lookupObject<volVectorField>("U");

            // Lookup liquid fraction alpha_liquid
            const volScalarField& alphaLiquid = mesh().lookupObject<volScalarField>("alpha.phase1");

            // Compute weight (= gaz fraction)
            volScalarField weight
            (
                IOobject
                (
                    "weight",
                    mesh().time().timeName(),
                    U.mesh(),
                    IOobject::NO_READ,
                    IOobject::NO_WRITE
                ),
                (1-alphaLiquid)
            );

            // Compute isosurface alpha=0.5
            dictionary isoSurfDict;
            isoSurfDict.add("type","isoSurfaceCell");
            isoSurfDict.add("isoField","alpha.phase1");
            isoSurfDict.add("isoValue",0.5);
            sampledIsoSurfaceCell isoInterface("isoInterface", mesh(), isoSurfDict);
            isoInterface.update();

            // Compute bubble volume
            dimensionedScalar bubbleVolume = fvc::domainIntegrate(weight); // This is integral of "weight" over the volume
            Info << "bubble volume     = " << bubbleVolume.value() << endl;

	        // Compute min and max position of the isosurface NOT WORKING
            /*
            const pointField& pf = isoInterface.points();
            scalar xmin =  100.0;
            scalar xmax = -100.0;
            forAll(pf, i)
            {
              xmin = min(pf[i][0],xmin);
              xmax = max(pf[i][0],xmax);
            }
            
            Info << "bubble xmin and xmax = " << xmin << " " << xmax << endl;
            Info << "bubble length = " << xmax-xmin << endl;
            */
            Info << "----------bubblepproc----------\n" << endl;

        #};
    }

}

// ************************************************************************* //
